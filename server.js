const express = require("express");
const mongoose = require("mongoose");
const users = require("./routes/api/users");
const posts = require("./routes/api/posts");
const profile = require("./routes/api/profile");
const db = require("./config/keys").mongoURI;

mongoose
  .connect(db)
  .then(() => {
    console.log("MongoDB Connected");
  })
  .catch(error => console.log(error));

// try {
//   (async () => {
//     await mongoose.connect(db);
//     console.log("MongoDB Connected");
//   })();
// } catch (error) {
//   () => console.log(error);
// }

const app = express();

app.get("/", (req, res) => res.send("Hello World!!"));

// Use Ports

app.use("/api/users", users);
app.use("/api/posts", posts);
app.use("/api/profile", profile);

const port = process.env.port || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
